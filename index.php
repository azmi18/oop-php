<?php
require_once ('animal.php');
require_once ('ape.php');
require_once ('frog.php');

$domba = new animal("Shaun");
$domba-> setkaki (4);
$domba-> setdarah ("no");

echo "Name : ". $domba->name."<br>";
echo "legs : ". $domba->getkaki()."<br>";
echo "cold_blooded : ". $domba->getdarah()."<br><br>";

$kodok = new frog("Buduk");
echo "Name : ". $kodok->name."<br>";
echo "legs : ". $kodok->legs."<br>";
echo "cold_blooded : ". $kodok->cold_blooded."<br>";
echo "jump : ";  $kodok->jump()."<br><br>";

$kong = new ape("Kera Sakti");
echo "<br><br>Name : ". $kong->name."<br>";
echo "legs : ". $kong->legs."<br>";
echo "cold_blooded : ". $kong->cold_blooded."<br>";
echo "yell : ";  $kong->yell();
?>